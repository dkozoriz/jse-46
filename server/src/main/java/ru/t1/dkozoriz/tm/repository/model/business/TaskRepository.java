package ru.t1.dkozoriz.tm.repository.model.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.model.business.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends BusinessRepository<Task> implements ru.t1.dkozoriz.tm.api.repository.model.ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}