package ru.t1.dkozoriz.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.ISessionDtoService;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;

public final class SessionDtoService extends UserOwnedDtoService<SessionDto> implements ISessionDtoService {

    private final static String NAME = "Session";

    @NotNull
    public String getName() {
        return NAME;
    }

    public SessionDtoService(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    protected ISessionDtoRepository getRepository(@NotNull EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

}